﻿using System.Text.Json.Serialization;

namespace Roulette.Domain.Core
{
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        [JsonIgnore]
        public string PasswordHash { get; set; }
        public bool IsEmailConfirm { get; set; }
        public string ReferralKey { get; set; }
        [JsonIgnore]
        public string TwoFactorSecret { get; set; }
        public bool IsTwoFactorEnabled { get; set; }
        public int ReferralUserId { get; set; }
        [JsonIgnore]
        public bool IsBot { get; set; }
        public string PhoneNumber { get; set; }
        public string Nickname { get; set; }
        public int AvatarNumber { get; set; }
    }
}
