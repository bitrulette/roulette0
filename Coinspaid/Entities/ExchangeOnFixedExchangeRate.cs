﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Coinspaid.Entities
{
    public class ExchangeOnFixedExchangeRateData
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "foreign_id")]
        public string ForeignId { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "sender_amount")]
        public string SenderAmount { get; set; }

        [JsonProperty(PropertyName = "sender_currency")]
        public string SenderCurrency { get; set; }

        [JsonProperty(PropertyName = "receiver_amount")]
        public string ReceiverAmount { get; set; }

        [JsonProperty(PropertyName = "receiver_currency")]
        public string ReceiverCurrency { get; set; }

        [JsonProperty(PropertyName = "fee_amount")]
        public string FeeAmount { get; set; }

        [JsonProperty(PropertyName = "fee_currency")]
        public string FeeCurrency { get; set; }

        [JsonProperty(PropertyName = "price")]
        public string Price { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
    }

    public class ExchangeOnFixedExchangeRateResponse : ApiResponse
    {
        [JsonProperty(PropertyName = "data")]
        public ExchangeOnFixedExchangeRateData Data { get; set; }
    }

    public class ExchangeOnFixedExchangeRateRequest : IApiRequest
    {
        [JsonProperty(PropertyName = "sender_currency")]
        public string SenderCurrency { get; set; }

        [JsonProperty(PropertyName = "receiver_currency")]
        public string ReceiverCurrency { get; set; }

        [JsonProperty(PropertyName = "sender_amount")]
        public string SenderAmount { get; set; }

        [JsonProperty(PropertyName = "foreign_id")]
        public string ForeignId { get; set; }

        [JsonProperty(PropertyName = "price")]
        public string Price { get; set; }
    }
}
