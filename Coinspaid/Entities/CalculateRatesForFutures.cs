﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Coinspaid.Entities
{
    public class CalculateRatesForFuturesData
    {
        [JsonProperty(PropertyName = "addresses")]
        public List<AddressData> Addresses { get; set; }

        [JsonProperty(PropertyName = "ts_fixed")]
        public int TsFixed { get; set; }

        [JsonProperty(PropertyName = "ts_release")]
        public int TsRelease { get; set; }

        [JsonProperty(PropertyName = "rates")]
        public Rates Rates { get; set; }
    }

    public class AddressData
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "currency")]
        public string Currency { get; set; }

        [JsonProperty(PropertyName = "convert_to")]
        public string ConvertTo { get; set; }

        [JsonProperty(PropertyName = "address")]
        public string Address { get; set; }

        [JsonProperty(PropertyName = "tag")]
        public object Tag { get; set; }

        [JsonProperty(PropertyName = "foreign_id")]
        public string ForeignId { get; set; }
    }

    public class BTCUSD
    {
        [JsonProperty(PropertyName = "5.00000000")]
        public string _500000000 { get; set; }

        [JsonProperty(PropertyName = "10.00000000")]
        public string _1000000000 { get; set; }

        [JsonProperty(PropertyName = "20.00000000")]
        public string _2000000000 { get; set; }

        [JsonProperty(PropertyName = "50.00000000")]
        public string _5000000000 { get; set; }

        [JsonProperty(PropertyName = "100.00000000")]
        public string _10000000000 { get; set; }

        [JsonProperty(PropertyName = "200.00000000")]
        public string _20000000000 { get; set; }

        [JsonProperty(PropertyName = "300.00000000")]
        public string _30000000000 { get; set; }
    }

    public class Rates
    {
        [JsonProperty(PropertyName = "BTCUSD")]
        public BTCUSD BTCUSD { get; set; }
    }
    public class CalculateRatesForFuturesResponse : ApiResponse
    {
        [JsonProperty(PropertyName = "data")]
        public CalculateRatesForFuturesData Data { get; set; }
    }

    public class CalculateRatesForFuturesRequest : IApiRequest
    {
        [JsonProperty(PropertyName = "address")]
        public string Address { get; set; }
    }
}
