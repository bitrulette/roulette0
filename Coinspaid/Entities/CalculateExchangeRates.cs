﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Coinspaid.Entities
{
    public class CalculateExchangeRatesData
    {
        [JsonProperty(PropertyName = "sender_amount")]
        public int SenderAmount { get; set; }

        [JsonProperty(PropertyName = "sender_currency")]
        public string SenderCurrency { get; set; }

        [JsonProperty(PropertyName = "receiver_amount")]
        public string ReceiverAmount { get; set; }

        [JsonProperty(PropertyName = "receiver_currency")]
        public string ReceiverCurrency { get; set; }

        [JsonProperty(PropertyName = "fee_amount")]
        public string FeeAmount { get; set; }

        [JsonProperty(PropertyName = "fee_currency")]
        public string FeeCurrency { get; set; }

        [JsonProperty(PropertyName = "price")]
        public string Price { get; set; }

        [JsonProperty(PropertyName = "ts_fixed")]
        public int TsFixed { get; set; }

        [JsonProperty(PropertyName = "ts_release")]
        public int TsRelease { get; set; }

        [JsonProperty(PropertyName = "fix_period")]
        public int FixPeriod { get; set; }
    }

    public class CalculateExchangeRatesResponse : ApiResponse
    {
        [JsonProperty(PropertyName = "data")]
        public CalculateExchangeRatesData Data { get; set; }
    }

    public class CalculateExchangeRatesRequest : IApiRequest
    {
        [JsonProperty(PropertyName = "receiver_amount")]
        public string ReceiverAmount { get; set; }

        [JsonProperty(PropertyName = "sender_currency")]
        public string SenderCurrency { get; set; }

        [JsonProperty(PropertyName = "receiver_currency")]
        public string ReceiverCurrency { get; set; }

        [JsonProperty(PropertyName = "sender_amount")]
        public string SenderAmount { get; set; }
    }
}
