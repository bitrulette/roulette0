﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Coinspaid.Entities
{
    public class ReceiveCryptocurrencyData
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "currency")]
        public string Currency { get; set; }

        [JsonProperty(PropertyName = "convert_to")]
        public string ConvertTo { get; set; }

        [JsonProperty(PropertyName = "address")]
        public string Address { get; set; }

        [JsonProperty(PropertyName = "tag")]
        public string Tag { get; set; }

        [JsonProperty(PropertyName = "foreign_id")]
        public string ForeignId { get; set; }
    }

    public class ReceiveCryptocurrencyResponse : ApiResponse
    {
        [JsonProperty(PropertyName = "data")]
        public ReceiveCryptocurrencyData Data { get; set; }
    }

    public class ReceiveCryptocurrencyRequest : IApiRequest
    {
        [JsonProperty(PropertyName = "foreign_id")]
        public string ForeignId { get; set; }

        [JsonProperty(PropertyName = "convert_to")]
        public string ConvertTo { get; set; }

        [JsonProperty(PropertyName = "currency")]
        public string Currency { get; set; }
    }
}
