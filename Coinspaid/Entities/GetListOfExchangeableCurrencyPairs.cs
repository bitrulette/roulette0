﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Coinspaid.Entities
{

    public class GetListOfExchangeableCurrencyPairsData
    {
        [JsonProperty(PropertyName = "currency_from")]
        public CurrencyFrom CurrencyFrom { get; set; }

        [JsonProperty(PropertyName = "currency_to")]
        public CurrencyTo CurrencyTo { get; set; }

        [JsonProperty(PropertyName = "rate_from")]
        public string RateFrom { get; set; }

        [JsonProperty(PropertyName = "rate_to")]
        public string RateTo { get; set; }
    }
    public class CurrencyFrom
    {
        [JsonProperty(PropertyName = "currency")]
        public string Currency { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "min_amount")]
        public string MinAmount { get; set; }

        [JsonProperty(PropertyName = "min_amount_deposit_with_exchange")]
        public string MinAmountDepositWithExchange { get; set; }
    }

    public class CurrencyTo
    {
        [JsonProperty(PropertyName = "currency")]
        public string Currency { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
    }

    public class GetListOfExchangeableCurrencyPairsResponse : ApiResponse
    {
        [JsonProperty(PropertyName = "data")]
        public List<GetListOfExchangeableCurrencyPairsData> Data { get; set; }
    }

    public class GetListOfExchangeableCurrencyPairsRequest:IApiRequest
    {
        [JsonProperty(PropertyName = "currency_from")]
        public string CurrencyFrom { get; set; }

        [JsonProperty(PropertyName = "currency_to")]
        public string CurrencyTo { get; set; }
    }
}
