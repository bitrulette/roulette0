﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Coinspaid.Entities
{
    public class GetListOfSupportedCurrenciesData
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "currency")]
        public string Currency { get; set; }

        [JsonProperty(PropertyName = "minimum_amount")]
        public string MinimumAmount { get; set; }

        [JsonProperty(PropertyName = "deposit_fee_percent")]
        public string DepositFeePercent { get; set; }

        [JsonProperty(PropertyName = "withdrawal_fee_percent")]
        public string WithdrawalFeePercent { get; set; }

        [JsonProperty(PropertyName = "precision")]
        public int Precision { get; set; }
    }

    public class GetListOfSupportedCurrenciesResponse : ApiResponse
    {
        [JsonProperty(PropertyName = "data")]
        public List<GetListOfSupportedCurrenciesData> Data { get; set; }
    }

    public class GetListOfSupportedCurrenciesRequest : IApiRequest
    {
        [JsonProperty(PropertyName = "visible")]
        public bool Visible { get; set; }
    }
}
