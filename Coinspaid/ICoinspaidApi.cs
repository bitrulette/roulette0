﻿using Coinspaid.Entities;
using System.Threading.Tasks;

namespace Coinspaid
{
    public interface ICoinspaidApi
    {
        Task<T> ApiQueryAsync<T>(string endpoint, IApiRequest payload) where T : ApiResponse, new();
        Task<CalculateExchangeRatesResponse> CalculateExchangeRatesAsync(string receiverAmount, string senderCurrency, string receiverCurrency, string senderAmount);
        Task<CalculateRatesForFuturesResponse> CalculateRatesForFuturesAsync(string address);
        Task<ConfirmFuturesTransactionResponse> ConfirmFuturesTransactionAsync(string address, string senderCurrency, string receiverCurrency, string receiverAmount);
        Task<ExchangeOnFixedExchangeRateResponse> ExchangeOnFixedExchangeRateAsync(string senderCurrency, string receiverCurrency, string senderAmount, string foreignId, string price);
        Task<ExchangeRegardlessTheExchangeRateResponse> ExchangeRegardlessTheExchangeRateAsync(string senderCurrency, string receiverCurrency, string senderAmount, string foreignId);
        Task<GetListOfBalancesResponse> GetListOfBalancesAsync();
        Task<GetListOfExchangeableCurrencyPairsResponse> GetListOfExchangeableCurrencyPairsAsync(string currencyFrom, string currencyTo);
        Task<GetListOfSupportedCurrenciesResponse> GetListOfSupportedCurrenciesAsync(bool visible = false);
        Task<ReceiveCryptocurrencyResponse> ReceiveCryptocurrencyAsync(string foreignId, string currency, string convertTo);
        Task<WithdrawCryptocurrencyResponse> WithdrawCryptocurrencyAsync(string foreignId, string amount, string currency, string convertTo, string address, string tag);
    }
}