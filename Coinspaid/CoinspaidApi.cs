﻿using Coinspaid.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Coinspaid
{
    public class CoinspaidApi : ICoinspaidApi
    {
        private string _baseUri { get; set; }
        private string _key { get; set; }
        private string _secret { get; set; }

        private static readonly HttpClient _client = new HttpClient();

        private const string GET_LIST_OF_SUPPORTED_CURRENCIES = "/api/v2/currencies/list";
        private const string GET_LIST_OF_EXCHANGEABLE_CURRENCY_PAIRS = "/api/v2/currencies/pairs";
        private const string GET_LIST_OF_BALANCES = "/api/v2/accounts/list";
        private const string RECEIVE_CRYPTOCURRENCY = "/api/v2/addresses/take";
        private const string WITHDRAW_CRYPTOCURRENCY = "/api/v2/withdrawal/crypto";
        private const string CALCULATE_EXCHANGE_RATES = "/api/v2/exchange/calculate";
        private const string EXCHANGE_ON_FIXED_EXCHANGE_RATE = "/api/v2/exchange/fixed";
        private const string EXCHANGE_REGARDLESS_THE_EXCHANGE_RATE = "/api/v2/exchange/now";
        private const string CALCULATE_RATES_FOR_FUTURES = "/api/v2/futures/rates";

        public CoinspaidApi(string baseUri, string key, string secret)
        {
            _baseUri = baseUri;
            _key = key;
            _secret = secret;
            _client.BaseAddress = new Uri(_baseUri);
            _client.DefaultRequestHeaders.Add("X-Processing-Key", _key);
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        }

        private string Hash(string message, string secretKey)
        {
            var encoding = System.Text.Encoding.UTF8;
            byte[] msgBytes = encoding.GetBytes(message);
            var keyBytes = encoding.GetBytes(secretKey);
            using (HMACSHA512 hmac = new HMACSHA512(keyBytes))
            {
                byte[] hashBytes = hmac.ComputeHash(msgBytes);

                var sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                    sb.Append(hashBytes[i].ToString("x2"));
                return sb.ToString();
            }
        }

        public async Task<T> ApiQueryAsync<T>(string endpoint, IApiRequest payload) where T : ApiResponse, new()
        {
            var message = JsonConvert.SerializeObject(payload);
            var sign = Hash(message, _secret);
            _client.DefaultRequestHeaders.Remove("X-Processing-Signature");
            _client.DefaultRequestHeaders.Add("X-Processing-Signature", sign);

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, endpoint)
            {
                Content = new StringContent(message, Encoding.UTF8, "application/json")
            };

            using (var result = await _client.SendAsync(request))
            {
                string content = await result.Content.ReadAsStringAsync();
                switch (result.StatusCode)
                {
                    case System.Net.HttpStatusCode.OK:
                        return JsonConvert.DeserializeObject<T>(content);
                    default:
                        var error = JsonConvert.DeserializeObject<ErrorData>(content);
                        return new T
                        {
                            Error = error
                        };
                }
            }
        }

        /// <summary>
        /// Get a list of all supported currencies
        /// </summary>
        /// <param name="visible">Allows to get a list of currently enabled/disabled currencies; default false</param>
        public async Task<GetListOfSupportedCurrenciesResponse> GetListOfSupportedCurrenciesAsync(bool visible = false)
        {
            var payload = new GetListOfSupportedCurrenciesRequest()
            {
                Visible = visible
            };
            return await ApiQueryAsync<GetListOfSupportedCurrenciesResponse>(GET_LIST_OF_SUPPORTED_CURRENCIES, payload);
        }

        /// <summary>
        /// Get list of currency exchange pairs.
        /// </summary>
        /// <param name="currencyFrom">Filter by currency ISO that exchanges from, example: BTC</param>
        /// <param name="currencyTo">Filter by currency ISO that can be converted to, example: EUR</param>
        public async Task<GetListOfExchangeableCurrencyPairsResponse> GetListOfExchangeableCurrencyPairsAsync(string currencyFrom, string currencyTo)
        {
            var payload = new GetListOfExchangeableCurrencyPairsRequest()
            {
                CurrencyFrom = currencyFrom,
                CurrencyTo = currencyTo
            };
            return await ApiQueryAsync<GetListOfExchangeableCurrencyPairsResponse>(GET_LIST_OF_EXCHANGEABLE_CURRENCY_PAIRS, payload);
        }

        /// <summary>
        /// Get list of all the balances (including zero balances).
        /// </summary>
        public async Task<GetListOfBalancesResponse> GetListOfBalancesAsync()
        {
            var payload = new GetListOfBalancesRequest();
            return await ApiQueryAsync<GetListOfBalancesResponse>(GET_LIST_OF_BALANCES, payload);
        }

        /// <summary>
        /// Take address for depositing crypto and (it depends on specified params) exchange from crypto to fiat on-the-fly.
        /// </summary>
        /// <param name="foreignId">Your info for this address, will returned as reference in Address responses, example: user-id:2048</param>
        /// <param name="currency">ISO of currency to receive funds in, example: BTC</param>
        /// <param name="convertTo">If you need auto exchange all incoming funds for example to EUR, specify this param as EUR or any other supported currency ISO, to see list of pairs see previous method.</param>
        /// <returns></returns>
        public async Task<ReceiveCryptocurrencyResponse> ReceiveCryptocurrencyAsync(string foreignId, string currency, string convertTo)
        {
            var payload = new ReceiveCryptocurrencyRequest()
            {
                ForeignId = foreignId,
                Currency = currency,
                ConvertTo = convertTo
            };
            return await ApiQueryAsync<ReceiveCryptocurrencyResponse>(RECEIVE_CRYPTOCURRENCY, payload);
        }

        /// <summary>
        /// Withdraw in crypto to any specified address. You can send Cryptocurrency from your Fiat currency balance by using "convert_to" parameter.
        /// </summary>
        /// <param name="foreignId">Unique foreign ID in your system, example: "122929"</param>
        /// <param name="amount">Amount of funds to withdraw, example: "1"</param>
        /// <param name="currency">Currency ISO to be withdrawn, example: "BTC"</param>
        /// <param name="convertTo">If you want to auto convert for example EUR to BTC, specify this param as ETH or any other currency supported (see list of exchangeable pairs API method).</param>
        /// <param name="address">Cryptocurrency address where you want to send funds.</param>
        /// <param name="tag">Tag (if it's Ripple or BNB) or memo (if it's Bitshares or EOS)</param>
        public async Task<WithdrawCryptocurrencyResponse> WithdrawCryptocurrencyAsync(string foreignId, string amount, string currency, string convertTo, string address, string tag)
        {
            var payload = new WithdrawCryptocurrencyRequest()
            {
                ForeignId = foreignId,
                Amount = amount,
                Currency = currency,
                ConvertTo = convertTo,
                Address = address,
                Tag = tag
            };
            return await ApiQueryAsync<WithdrawCryptocurrencyResponse>(WITHDRAW_CRYPTOCURRENCY, payload);
        }

        /// <summary>
        /// Get info about exchange rates.
        /// This endpoint has limitation up to 30 requests per minute from one IP address.
        /// </summary>
        /// <param name="receiverAmount">Amount you want to calculate for getting, example: "10". The parameter is required when the "sender_amount" parameter is absent</param>
        /// <param name="senderCurrency">Currency ISO for which you want to calculate the exchange rate, example: "BTC"</param>
        /// <param name="receiverCurrency">Currency ISO to be exchanged, example: "EUR"</param>
        /// <param name="senderAmount">Amount you want to calculate, example: "3". The parameter is required when the "receiver_amount" parameter is absent</param>
        /// <returns></returns>
        public async Task<CalculateExchangeRatesResponse> CalculateExchangeRatesAsync(string receiverAmount, string senderCurrency, string receiverCurrency, string senderAmount)
        {
            var payload = new CalculateExchangeRatesRequest()
            {
                ReceiverAmount = receiverAmount,
                SenderCurrency = senderCurrency,
                ReceiverCurrency = receiverCurrency,
                SenderAmount = senderAmount
            };
            return await ApiQueryAsync<CalculateExchangeRatesResponse>(CALCULATE_EXCHANGE_RATES, payload);
        }

        /// <summary>
        /// Make exchange on a given fixed exchange rate.
        /// </summary>
        /// <param name="senderCurrency">Currency ISO which you want to exchange, example: "LTC"</param>
        /// <param name="receiverCurrency">Currency ISO to be exchanged, example: "USD"</param>
        /// <param name="senderAmount">Amount you want to exchange, example: "6.5"</param>
        /// <param name="foreignId">Unique foreign ID in your system, example: "134453"</param>
        /// <param name="price">Exchange rate price on which exchange will be placed, example: "89.75202000"</param>
        /// <returns></returns>
        public async Task<ExchangeOnFixedExchangeRateResponse> ExchangeOnFixedExchangeRateAsync(string senderCurrency, string receiverCurrency, string senderAmount, string foreignId, string price)
        {
            var payload = new ExchangeOnFixedExchangeRateRequest()
            {
                SenderCurrency = senderCurrency,
                ReceiverCurrency = receiverCurrency,
                SenderAmount = senderAmount,
                ForeignId = foreignId,
                Price = price
            };
            return await ApiQueryAsync<ExchangeOnFixedExchangeRateResponse>(EXCHANGE_ON_FIXED_EXCHANGE_RATE, payload);
        }

        /// <summary>
        /// Make exchange on a given fixed exchange rate.
        /// </summary>
        /// <param name="senderCurrency">Currency ISO which you want to exchange, example: "LTC"</param>
        /// <param name="receiverCurrency">Currency ISO to be exchanged, example: "USD"</param>
        /// <param name="senderAmount">Amount you want to exchange, example: "6.5"</param>
        /// <param name="foreignId">Unique foreign ID in your system, example: "134453"</param>
        /// <param name="price">Exchange rate price on which exchange will be placed, example: "89.75202000"</param>
        public async Task<ExchangeRegardlessTheExchangeRateResponse> ExchangeRegardlessTheExchangeRateAsync(string senderCurrency, string receiverCurrency, string senderAmount, string foreignId)
        {
            var payload = new ExchangeRegardlessTheExchangeRateRequest()
            {
                SenderCurrency = senderCurrency,
                ReceiverCurrency = receiverCurrency,
                SenderAmount = senderAmount,
                ForeignId = foreignId
            };
            return await ApiQueryAsync<ExchangeRegardlessTheExchangeRateResponse>(EXCHANGE_REGARDLESS_THE_EXCHANGE_RATE, payload);
        }

        /// <summary>
        /// Get info about rates for futures.
        /// </summary>
        /// <param name="address">Exchange address for which you want to calculate futures' rates</param>
        public async Task<CalculateRatesForFuturesResponse> CalculateRatesForFuturesAsync(string address)
        {
            var payload = new CalculateRatesForFuturesRequest()
            {
                Address = address
            };
            return await ApiQueryAsync<CalculateRatesForFuturesResponse>(CALCULATE_RATES_FOR_FUTURES, payload);
        }

        /// <summary>
        /// Confirm futures transaction.
        /// </summary>
        /// <param name="address">Exchange address for which you want to confirm futures</param>
        /// <param name="senderCurrency">Currency ISO which you want to exchange, example: "BTC"</param>
        /// <param name="receiverCurrency">Currency ISO to be exchanged, example: "EUR"</param>
        /// <param name="receiverAmount">Amount you want to receive</param>
        public async Task<ConfirmFuturesTransactionResponse> ConfirmFuturesTransactionAsync(string address, string senderCurrency, string receiverCurrency, string receiverAmount)
        {
            var payload = new ConfirmFuturesTransactionRequest()
            {
                Address = address,
                SenderCurrency = senderCurrency,
                ReceiverCurrency = receiverCurrency,
                ReceiverAmount = receiverAmount
            };
            return await ApiQueryAsync<ConfirmFuturesTransactionResponse>(EXCHANGE_ON_FIXED_EXCHANGE_RATE, payload);
        }/**/
    }
}
