﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roulette_backend.Model
{
    public class Address
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Value { get; set; }
        public string Currency { get; set; }
        public bool IsBlocked { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
