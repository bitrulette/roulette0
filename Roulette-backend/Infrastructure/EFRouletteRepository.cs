﻿using Microsoft.EntityFrameworkCore;
using Roulette.Domain.Core;
using Roulette_backend.Interface;
using Roulette_backend.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roulette_backend.Infrastructure
{
    public class EFRouletteRepository : IRouletteRepository
    {
        readonly ApplicationDbContext context;
        public EFRouletteRepository(ApplicationDbContext ctx) => context = ctx;

        public IQueryable<User> Users => context.Users;

        public async Task<User> GetUser(string email, string passwordHash)
        {
            return await context.Users.Where(x => x.Email == email && x.PasswordHash == passwordHash)/*.DefaultIfEmpty(null)*/.FirstOrDefaultAsync();// Async();
        }
    }
}
