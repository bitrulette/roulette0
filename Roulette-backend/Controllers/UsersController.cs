﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Roulette.Domain.Core;
using Roulette_backend.Helpers;
using Roulette_backend.Interface;
using Roulette_backend.Model;
using Roulette_backend.Services;

namespace Roulette_backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;
        readonly IRouletteRepository repository;

        public UsersController(IRouletteRepository repo/*, IUserService userService*/)
        {
            //_userService = userService;
            repository = repo;
        }

        [HttpPost("authenticate")]
        public IActionResult Authenticate(AuthenticateRequest model)
        {
            /*var response = _userService.Authenticate(model);

            if (response == null)
                return BadRequest(new { message = "Username or password is incorrect" });
            */
            return Ok(/*response*/);
        }

        [Authorize]
        [HttpPut("balanceup")]
        public string BalanceUp(/*int value, string currency*/)
        {
            /*var user = (User)HttpContext.Items["User"];
            return user.Id + " " + user.Nickname;*/
            return "authorized";
        }
    }
}
