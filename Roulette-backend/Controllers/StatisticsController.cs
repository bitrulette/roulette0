﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Roulette_backend.Model;

namespace Roulette_backend.Controllers
{
    [ApiController]
    public class StatisticsController : ControllerBase
    {
        [HttpGet("Statistic")]
        public ResponseData Get([FromQuery] string currency)
        {
            var s = new
            {
                games_today = 10,
            max_bet = 20,
            online_users = 30,
            paid_total = 40,
            top_jackpot = 50,
            top_luck = 5 * 100,
        };
            return ResponseData.Create(s, "ok", "ok");
        }
    }
}
