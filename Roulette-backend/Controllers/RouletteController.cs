﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Roulette_backend.Model;

namespace Roulette_backend.Controllers
{
    [Route("[controller]")]
    [Authorize]
    public class RouletteController : Controller
    {
        public class result {
                public string id { get; set; }
        public string currency { get; set; }
        public IEnumerable<users> members { get; set; }
        public int amount { get; set; }

        public int game_time { get; set; }
    }

    public class users
        {
          public string user_id { get; set; }
            public string amount{ get; set; }
            public string nickname{ get; set; }
            public string avatar_number{ get; set; }
        }

        public class response
        {
            public Data data { get; set; }
        }

        [HttpGet("status")]
        public response Status([FromQuery] string type, [FromQuery] string duration)
        {
            //var res = new result();

            var s = new List<users>() {
                new users(){amount = "10", avatar_number = "10", nickname = "nik1", user_id = "10" },
                new users(){amount = "11", avatar_number = "11", nickname = "nik2", user_id = "20" },
                new users(){amount = "12", avatar_number = "12", nickname = "nik3", user_id = "30" },
            };
            //res.members = s;
            //var user = (User)HttpContext.Items["User"];

            var res =  new response() { data = new Data() { data = "qwerty", status = "ok" } };
            return res;
        }
    }
}
