﻿using Roulette.Domain.Core;
using Roulette_backend.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roulette_backend.Interface
{
    public interface IRouletteRepository
    {
        IQueryable<User> Users { get;}

        Task<User> GetUser(string email, string passwordHash);
    }
}
