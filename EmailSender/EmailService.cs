﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace EmailSender
{
    public class EmailService
    {
        /*
http://5.45.116.138/postfixadmin/public/login.php
admin@gamebitco.in \ pc1h49xNVvfYv#Xm*q*bMt@9

SMTP:
mail.gamebitco.in
 port 465

IMAP
mail.gamebitco.in
port 993

        info@gamebitco.in \ K5I#5CAa0yb^I3laNSD^q1h4
        */

        private const string HOST = "mail.gamebitco.in";
        private const int PORT = 25;
        private const bool USESSL = false;
        private const string LOGIN = "info@gamebitco.in";
        private const string PASSWORD = "K5I#5CAa0yb^I3laNSD^q1h4";


        private async Task SendEmailAsync(string fromEmail, string fromName, string toEmail,  string subject, string message)
        {
             var emailMessage = new MimeMessage();

             emailMessage.From.Add(new MailboxAddress(fromName, fromEmail));
             emailMessage.To.Add(new MailboxAddress("qwwerty", toEmail));
             emailMessage.Subject = subject;
             emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
             {
                 Text = message
             };

             using (var client = new MailKit.Net.Smtp.SmtpClient())
             {
                client.ServerCertificateValidationCallback = (a, b, c, d) => true;
                 //client.Connect(HOST, PORT);
                 await client.ConnectAsync(HOST, PORT, USESSL);
                 //await client.ConnectAsync("mail.gamebitco.in", 25, SecureSocketOptions.StartTls);
                 await client.AuthenticateAsync(LOGIN, PASSWORD);
                 await client.SendAsync(emailMessage);

                 await client.DisconnectAsync(true);
             }
        }

        public async Task SendConfirmEmail(string user, int code)
        {
            await SendEmailAsync(LOGIN, LOGIN, user, "рџЋ® Welcome to Crypto Roulette", $"Your activation code is < b >{code}</ b >.Or click by<a href= >link</a>");
        }
    }
}
